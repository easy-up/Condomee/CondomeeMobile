/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @typescript
 */

import React from 'react'
import { Root } from "native-base";
import { DrawerNavigator } from "react-navigation"

const App = DrawerNavigator(
    {
        
    },
    {
        initialRouteName: 'Home',
    }
)

export default () =>
    <Root>
        <App />
    </Root>;
